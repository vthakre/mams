var app = angular.module("homePage", []);
app
		.controller(
				'homePageCtrl',
				[
						'$scope',
						'$http',
						function($scope, $http) {

							$scope.availableTags = [ "Select User Type",
									"Account Manager", "Financial Manager",
									"Director", "Tender Team", "Operation Manager" ];
							$scope.selectedavailableTags = $scope.availableTags[0];

							$scope.usertype = "";
							$scope.ad = {};
							$scope.checked = false;
							$scope.loading = false;

							$scope.createuser = function() {
								$scope.checked = true;
								$scope.loading = true;
								$scope.invalidMobile = false;
								$scope.invalidEmail = false;

								var mobile = "" + $scope.ad.mobNo;

								if (mobile.length < 10) {
									$scope.invalidMobile = true;
									$scope.checked = false;
									$scope.loading = false;
									return;
								} else if ($scope.selectedavailableTags == $scope.availableTags[0]) {
									alert("select user type");
									$scope.checked = false;
									$scope.loading = false;
									return;
								} else {
									$scope.ad.email = document
											.getElementById("email").value;
									var x = $scope.ad.email;
									var atpos = x.indexOf("@");
									var dotpos = x.lastIndexOf(".");
									if (atpos < 1 || dotpos < atpos + 2
											|| dotpos + 2 >= x.length) {
										$scope.invalidEmail = true;
										return;
									}
								}

								var api = "";
								if ($scope.selectedavailableTags == $scope.availableTags[1]) {
									$scope.usertype = "ACCOUNT_MANAGER";
									api = "/AmUser";
								} else if ($scope.selectedavailableTags == $scope.availableTags[2]) {
									$scope.usertype = "FINANCE_MANAGER";
									api = "/FmUser";
								} else if ($scope.selectedavailableTags == $scope.availableTags[3]) {
									$scope.usertype = "DIRECTOR";
									api = "/directorUser";
								}  else if ($scope.selectedavailableTags == $scope.availableTags[4]) {
									$scope.usertype = "TENDER_TEAM";
									api = "/tenderTeamUser";
								}  else if ($scope.selectedavailableTags == $scope.availableTags[5]) {
									$scope.usertype = "OP_MANAGER";
									api = "/opManagerUser";
								} else {
									alert("Select User Type")
									return;
								}


								var postData = {
									fullName : $scope.ad.fullName,
									email : $scope.ad.email,
									mobNo : $scope.ad.mobNo,
									userRole : '' + $scope.usertype
								}
								
								$http({
									method : 'POST',
									url : '' + api,
									data : postData,
									headers : {
										"Content-Type" : "application/json",
										"Accept" : "text/plain"
									}
								})
										.success(
												function(data, status) {
													$scope.checked = false;
													$scope.loading = false;
													console.log(postData);
													$scope.ad.fullName = "";
													$scope.ad.email = "";
													$scope.ad.mobNo = "";
													$scope.selectedavailableTags = $scope.availableTags[0];
													alert(data);
												})
										.error(
												function(data, status) {
													$scope.checked = false;
													$scope.loading = false;
													console.log(postData);
													$scope.ad.fullName = "";
													$scope.ad.email = "";
													$scope.ad.mobNo = "";
													$scope.selectedavailableTags = $scope.availableTags[0];
													alert(data);
												});

							}

						} ]);

app.directive('positiveInteger', function() {
	return {
		restrict : 'A',
		require : 'ngModel',
		link : function(scope, element, attrs, ctrl) {
			ctrl.$parsers.unshift(function(viewValue) {
				var INTEGER_REGEXP = /^\d+$/;
				if (INTEGER_REGEXP.test(viewValue)) { // it is valid
					ctrl.$setValidity('positiveInteger', true);
					return viewValue;
				} else { // it is invalid, return undefined (no model update)
					ctrl.$setValidity('positiveInteger', false);
					return undefined;
				}
			});
		}
	};
});
app.directive('emailValidate', function() {
	return {
		restrict : 'A',
		require : 'ngModel',
		link : function(scope, element, attrs, ctrl) {
			ctrl.$parsers.unshift(function(viewValue) {

				scope.ad.email = viewValue;
				console.log(scope.ad.email);

				scope.invalidEmail = false;

				var x = viewValue;
				var atpos = x.indexOf("@");
				var dotpos = x.lastIndexOf(".");
				if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
					scope.invalidEmail = true;

				} else {
					scope.invalidEmail = false;
				}
			});
		}
	};
});
