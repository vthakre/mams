var app = angular.module('createProject', []);

var array = [];

app.controller('tagsCtrl', function ( $scope,$http,$filter ) {
	
	$scope.createProject = {}
	$scope.createProject.tenderId = ""+localStorage.getItem("tenderId");
    $scope.tags = [];
	var postData = {};
    //$scope.allTags = [ 'bootstrap', 'list', 'angular', 'directive', 'edit', 'label', 'modal', 'close', 'button', 'grid', 'javascript', 'html', 'badge', 'dropdown'];
    
    $scope.allTags = [];
    
    $scope.createProject.costs = $filter('date')($scope.createProject.costs, "dd/MM/yyyy");
                      
    $http({
		method : 'GET',
		url : '/getAssociateUserListForOP',
		headers : {
			'Content-Type' : 'application/json'
		}
	}).success(function(data, status) {
		for(var i=0;i<data.length;i++){
			$scope.allTags.push(data[i].associateName);
			array.push(data[i].associateName);
		}
		console.log($scope.allTags);
	}).error(function(data, status) {
		alert("error" + data);
	});
    
    
    /*****************************************************************/
   $scope.createProject.revenues = [{revMilestone: 'Milestone 1',revDateP: '',revAmountP: 0,revDateA: '',revAmountA: 0}];
   	$scope.createProject.costs = [{cosMilestone: 'Milestone 1',cosDateP: '',cosAmountP: 0,cosDateA: '',cosAmountA: 0}];
   	$scope.addInputRevenue = function() {
           $scope.createProject.revenues.push({revMilestone: 'Milestone ',revDateP: '',revAmountP: 0,revDateA: '',revAmountA: 0});
   	};
       $scope.addInputCost = function() {
       	$scope.createProject.costs.push({cosMilestone: 'Milestone ',cosDateP: '',cosAmountP: 0,cosDateA: '',cosAmountA: 0});
   	};
   	$scope.removeRevenue = function(index) {
   		$scope.createProject.revenues.splice( index.$index, 1 );
   	};
   	$scope.removeCost = function(index) {
   		$scope.createProject.costs.splice( index.$index, 1 );
   	};    
       
   	 /*****************************************************************/
   	
   	 $scope.submit = function() {
   		 	$scope.createProject.nameOfAssociate = $scope.tags;
   		 	
   		 $scope.createProject.projectStartDate = $scope.createProject.projectStartDate.toLocaleString().split(',')[0];
   		 $scope.createProject.projectEndDate = $scope.createProject.projectEndDate.toLocaleString().split(',')[0];
   		 	
	for(var i = 0;i<$scope.createProject.costs.length;i++){   		 		
	   		 	$scope.createProject.costs[i].cosDateP = $scope.createProject.costs[i].cosDateP.toLocaleString().split(',')[0];
	   		 	$scope.createProject.costs[i].cosDateA = $scope.createProject.costs[i].cosDateA.toLocaleString().split(',')[0];
   		 	}
	for(var i = 0;i<$scope.createProject.revenues.length;i++){	 		
		 	$scope.createProject.revenues[i].revDateP = $scope.createProject.revenues[i].revDateP.toLocaleString().split(',')[0];
		 	$scope.createProject.revenues[i].revDateA = $scope.createProject.revenues[i].revDateA.toLocaleString().split(',')[0];
	 	}
	 	
   	    	console.log($scope.createProject);
   	    	

   			postData = $scope.createProject;

   			$http({
   				method : 'POST',
   				url : '/createProject',
   				data : postData,
   				headers : {
   					"Content-Type" : "application/json"
   				}
   			}).success(function(data, status) {
   				console.log(data);
   				alert(data);
   			}).error(function(data, status) {
   				console.log(data);
   				alert(data)
   			});
   		};
   		
   		
   		$scope.getrevenuepTotal = function() {
   		  var total = 0;
   		  for (var i = 0; i < $scope.createProject.revenues.length; i++) {

   		   total = $scope.createProject.revenues[i].revAmountP + total;

   		  }

   		  return total;
   		 };
   		 $scope.getrevenueaTotal = function() {
   		  var total = 0;

   		  for (var i = 0; i < $scope.createProject.revenues.length; i++) {

   		   total = $scope.createProject.revenues[i].revAmountA + total;

   		  }

   		  // console.log(total);
   		  return total;
   		 };

   		 $scope.getpTotal = function() {
   		  var total = 0;

   		  for (var i = 0; i < $scope.createProject.costs.length; i++) {

   		   total = $scope.createProject.costs[i].cosAmountP + total;

   		  }

   		  return total;
   		 };
   		 $scope.getaTotal = function() {
   		  var total = 0;
   		  // $scope.createProject.nameOfAssociate = $scope.tags;

   		  for (var i = 0; i < $scope.createProject.costs.length; i++) {

   		   total = $scope.createProject.costs[i].cosAmountA + total;

   		  }
   		  ($scope.createProject);
   		  // console.log(total);
   		  return total;
   		 };
   		 
   		$scope.getPlannedProfit = function() {
   	      var total = 0;

   	      total = $scope.getrevenuepTotal()-$scope.getpTotal();

   	      // console.log(total);
   	      $scope.createProject.plannedProfit = total;
   	      return total;
   	     };
   	     
   	     $scope.getActualProfit = function() {
   	      var total2 = 0;

   	      total2 = $scope.getrevenueaTotal()-$scope.getaTotal();

   	      // console.log(total);
   	      $scope.createProject.actualProfit = total2;
   	      return total2;
   	     };

   	});


app.directive('tagManager', function() {
    return {
        restrict: 'E',
        scope: {
          tags: '=',
          autocomplete: '=autocomplete'
        },
        template:'<div class="tags">' +
      			'<div style="padding:5px;margin:10px;" ng-repeat="(idx, tag) in tags" class="tag label label-success">{{tag}} <a style="color:black" href ng-click="remove(idx)">x</a></div>' +
            '</div>' +
            '<div class="input-group" style="width: 100%;"><input style="height: 30px;" type="text" class="form-control" placeholder="Add Associates..." ng-model="newValue" required/> ' +
            '<span class="input-group-btn"><a class="btn btn-default" ng-click="add()">Add</a></span></div>',
        link: function ( $scope, $element ) {
            
      		var input = angular.element($element).find('input');

      		// setup autocomplete
      		if ($scope.autocomplete) {
              $scope.autocompleteFocus = function(event, ui) {
                input.val(ui.item.value);
                return false;
              };
              $scope.autocompleteSelect = function(event, ui) {
                $scope.newValue = ui.item.value;
                $scope.$apply( $scope.add );
                
                return false;
              };
              $($element).find('input').autocomplete({
                    minLength: 0,
                    source: function(request, response) {
                      var item;
                      return response((function() {
                        var _i, _len, _ref, _results;
                        _ref = $scope.autocomplete;
                        _results = [];
                        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                          item = _ref[_i];
                          if (item.toLowerCase().indexOf(request.term.toLowerCase()) !== -1) {
                            _results.push(item);
                          }
                        }
                        return _results;
                      })());
                    },
                    focus: (function(_this) {
                      return function(event, ui) {
                        return $scope.autocompleteFocus(event, ui);
                      };
                    })(this),
                    select: (function(_this) {
                      return function(event, ui) {
                        return $scope.autocompleteSelect(event, ui);
                      };
                    })(this)
                  });
            }	
                  

             // adds the new tag to the array
            $scope.add = function() {
  				// if not dupe, add it
  				if ($scope.tags.indexOf($scope.newValue)==-1){
  					
  					if(checkTag($scope.newValue)){
  						$scope.tags.push( $scope.newValue );
  					}else {
						alert("Asssociate name not available")
					}
                }
                $scope.newValue = "";
            };
            
         // check item in tags
            function checkTag(value) {
            	for(var i = 0;i < array.length;i++){
            		if(array[i] == value){
            			return true;
            		}
            	}
            	return false;
            };
            
         // remove an item
            $scope.remove = function ( idx ) {
                $scope.tags.splice( idx, 1 );
            };
            
            // capture keypresses
            input.bind( 'keypress', function ( event ) {
                // enter was pressed
                if ( event.keyCode == 13 ) {
                    $scope.$apply( $scope.add );
                }
            });
        }
    };
});



//$(document).ready(function() {});