package com.mscs.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.mscs")
public class WebConfiguration extends WebMvcConfigurerAdapter
{

     /*
      * @Bean(name="HelloWorld") public ViewResolver viewResolver() {
      * InternalResourceViewResolver viewResolver = new
      * InternalResourceViewResolver();
      * viewResolver.setViewClass(JstlView.class);
      * viewResolver.setPrefix("/WEB-INF/views/");
      * viewResolver.setSuffix(".html");
      * 
      * return viewResolver; }
      */
     
     @Bean
     public ViewResolver viewResolver() {
         InternalResourceViewResolver resolver = new InternalResourceViewResolver();
         resolver.setPrefix("/resources/**");
         resolver.setSuffix(".html");
         return resolver;
     }

     @Override
     public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
         configurer.enable();
     }


     @Bean(name = "multipartResolver")
     public StandardServletMultipartResolver resolver() {
	  return new StandardServletMultipartResolver();
     }

     @Override
     public void addResourceHandlers(ResourceHandlerRegistry registry) {
	  registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
     }

}