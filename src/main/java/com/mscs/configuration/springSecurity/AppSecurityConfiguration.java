package com.mscs.configuration.springSecurity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import com.mscs.app.security.AjaxAuthenticationSuccessHandler;
import com.mscs.app.security.CustomFailureHandler;
import com.mscs.app.security.SecurityUserDetailsService;

@Configuration
@EnableWebSecurity
@ComponentScan({ "com.mscs.app.security" })
public class AppSecurityConfiguration extends WebSecurityConfigurerAdapter {
	@Autowired
	private SecurityUserDetailsService userDetailsService;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		// TODO Auto-generated method stub
	
		auth.userDetailsService(userDetailsService).passwordEncoder(new BCryptPasswordEncoder());
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// TODO Auto-generated method stub
		http.authorizeRequests()
				.antMatchers(HttpMethod.POST, "/associateUser").permitAll()
				.antMatchers(HttpMethod.POST, "/uploadFiles").permitAll()
				.antMatchers(HttpMethod.GET, "/get/**").permitAll()
				.antMatchers(HttpMethod.POST, "/adminUser").permitAll()
				.antMatchers(HttpMethod.POST, "/getFilesOfUser").permitAll()
				.antMatchers(HttpMethod.POST, "/forgot_password","/user/checkTokan","/reset_password").permitAll()

				.antMatchers(HttpMethod.GET,"/loginAssociateUser" ).access("hasRole('ROLE_ASSOCIATE')")
				.antMatchers(HttpMethod.GET, "/getAssociateUser/**").hasAnyAuthority("ROLE_ADMIN","ROLE_ACCOUNT_MANAGER","ROLE_FINANCE_MANAGER","ROLE_DIRECTOR")//access("hasRole('ROLE_ADMIN') and hasRole('ROLE_ACCOUNT_MANAGER') and hasRole('ROLE_FINANCE_MANAGER') and hasRole('ROLE_DIRECTOR') ")
	                        .antMatchers(HttpMethod.GET, "/getAssociateUserListForAM","/loginAM").access("hasRole('ROLE_ACCOUNT_MANAGER')")
	                        .antMatchers(HttpMethod.GET, "/getAssociateUserListForFM","/loginFM").access("hasRole('ROLE_FINANCE_MANAGER')")
	                        .antMatchers(HttpMethod.GET, "/getAssociateUserListForD","/loginDirector").access("hasRole('ROLE_DIRECTOR')")
	                        .antMatchers(HttpMethod.GET,"/getTenderList").access("hasRole('ROLE_OP_MANAGER')")
	                        .antMatchers(HttpMethod.GET,"/getAssociateUserListForOP" ).hasAnyAuthority("ROLE_DIRECTOR","ROLE_OP_MANAGER")

				.antMatchers(HttpMethod.POST, "/FmUser","/AmUser","/directorUser","/loginAdmin","/opManagerUser","/tenderTeamUser").access("hasRole('ROLE_ADMIN')")
                                .antMatchers(HttpMethod.POST, "/approveAssociateUserByFM").access("hasRole('ROLE_FINANCE_MANAGER')")
                                .antMatchers(HttpMethod.POST, "/approveAssociateUserByAM").access("hasRole('ROLE_ACCOUNT_MANAGER')")
                                .antMatchers(HttpMethod.POST, "/storeTender").access("hasRole('ROLE_TENDER_TEAM')")
                                .antMatchers(HttpMethod.POST, "/getTenderById","/createProject").access("hasRole('ROLE_OP_MANAGER')")

				.antMatchers("/resources/public/**").permitAll()
				.antMatchers("/resources/Admin/**").access("hasRole('ROLE_ADMIN')")
				.antMatchers("/resources/Director/**").access("hasRole('ROLE_DIRECTOR')")
				.antMatchers("/resources/financialManagement/**").access("hasRole('ROLE_FINANCE_MANAGER')")
				.antMatchers("/resources/accountManager/**").access("hasRole('ROLE_ACCOUNT_MANAGER')")
				.antMatchers("/resources/associate/**").access("hasRole('ROLE_ASSOCIATE')")
				.antMatchers("/resources/oprationManager/**").permitAll()//.access("hasRole('ROLE_OP_MANAGER')")
				.antMatchers("/resources/tenderMaster/**").access("hasRole('ROLE_TENDER_TEAM')")

				.anyRequest().authenticated().and().formLogin().loginProcessingUrl("/authenticate")
				.usernameParameter("username").passwordParameter("password")
				.successHandler(new AjaxAuthenticationSuccessHandler(new SavedRequestAwareAuthenticationSuccessHandler()))
				.failureHandler(new CustomFailureHandler())
				.permitAll().loginPage("/resources/public/index.html").and().httpBasic().and().logout()
				.logoutUrl("/logout").logoutSuccessUrl("/resources/public/index.html").and()
				.exceptionHandling().accessDeniedPage("/resources/public/accessDenied.html").and().csrf().disable();
	}

}
