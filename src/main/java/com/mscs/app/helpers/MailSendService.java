package com.mscs.app.helpers;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.task.TaskExecutor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
@PropertySource("classpath:email.properties")
public class MailSendService
{

     @Autowired
     private Environment	 env;
     @Autowired
     private JavaMailSender	 mailSender;
     @Autowired
     private TaskExecutor	 taskExecutor;

     private static final Logger LOGGER	= Logger.getLogger(MailSendService.class);

     /**
      * @param -
      *             message
      * @param -
      *             email_id
      * @param -
      *             subject
      */
     public void sendMail(String mailAddress, String htmlMessage, String subject) throws Exception {
	  taskExecutor.execute(new Runnable() {
	       public void run() {
		    try {
			 sendMessage(mailAddress, htmlMessage, subject);
		    } catch (Exception e) {
			 e.printStackTrace();
			 LOGGER.error("Failed to send email to: " + mailAddress + " reason: " + e.getMessage());
		    }
	       }
	  });
     }

     private void sendMessage(final String emailId, final String htmlMessage, final String subject) {

	  Properties jMailProps = new Properties();
	  jMailProps.put("mail.smtp.auth", true);
	  jMailProps.put("mail.smtp.starttls.enable", true);
	  jMailProps.put("mail.smtp.host", env.getProperty("smtp.host"));
	  jMailProps.put("mail.smtp.port", env.getProperty("smtp.port", Integer.class));

	  // Get the Session object.
	  Session session = Session.getInstance(jMailProps, new javax.mail.Authenticator() {
	       protected PasswordAuthentication getPasswordAuthentication() {
		    return new PasswordAuthentication(env.getProperty("smtp.username"),
	                      env.getProperty("smtp.password"));
	       }
	  });

	  try {
	       // Create a default MimeMessage object.
	       Message message = new MimeMessage(session);

	       // Set From: header field of the header.
	       message.setFrom(new InternetAddress(env.getProperty("support.email")));

	       // Set To: header field of the header.
	       message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailId));

	       // Set Subject: header field
	       message.setSubject(subject);

	       // Send the actual HTML message, as big as you like
	       message.setContent(htmlMessage, "text/html");

	       // Send message
	       Transport.send(message);

	       System.out.println("Sent HTML message successfully....");

	  } catch (MessagingException e) {
	       e.printStackTrace();
	       throw new RuntimeException(e);
	  }
     }
}
