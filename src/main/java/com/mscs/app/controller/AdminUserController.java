package com.mscs.app.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.mscs.app.dto.NewUserDTO;
import com.mscs.app.helpers.MailSendService;
import com.mscs.app.helpers.RandomPasswordGenerator;
import com.mscs.app.services.UserService;

/**
 * This controller is for admin related rest calls
 * 
 * @author himmat
 * 
 */
@Controller
@ComponentScan({ "com.mscs.app.helpers" })
public class AdminUserController
{

     private static final Logger LOGGER	= Logger.getLogger(AdminUserController.class);

     @Autowired
     UserService	     userService;
     @Autowired
     private JavaMailSender  mailSender;
     @Autowired
     private MailSendService mailSendService;
     @Autowired
     private Environment     env;

     /**
      * this rest used for register Admin
      * 
      * @param user
      * @param request
      * @param response
      * @throws Exception
      */
     @ResponseStatus(HttpStatus.OK)
     @RequestMapping(value = "/adminUser", method = RequestMethod.POST)
     public void createUser(@RequestBody NewUserDTO user, final HttpServletRequest request, HttpServletResponse response) throws Exception {
	  System.out.println("Admin user contrl called successfully.");

	  // Generate random username and password
	  String username = user.getEmail().trim();
	  String password = RandomPasswordGenerator.getRandomPassword().trim();
	  String encodedPassword = new BCryptPasswordEncoder().encode(password);

	  System.out.println("Admin user username===== "+username);
	  System.out.println("Admin user password===== "+password);
	  userService.createAdminUser(user.getrAddressLine1(), user.getrAddressLine2(), user.getrState(), user.getrTown(), user.getrPincode(),
	            username, encodedPassword, user.getUserRole(), user.getMobNo(), true, user.getFullName(), user.getEmail());

	  // generaate html message for registration
	  String htmlMessage = createHtmlPageforRegistration(user, username, password, user.getFullName());
	  // send the email with html message
	  final String subject = "Registration Successful";

	  // send mail throw another thread
	  mailSendService.sendMail(user.getEmail(), htmlMessage, subject);

	  // send back response string
	  PrintWriter out = response.getWriter();
	  out.print("user registered successfully");
	  out.flush();

     }

     /**
      * this method is used to construct html message
      * 
      * @param user
      * @param username
      * @param password
      * @param associateName
      * @return
      */
     private String createHtmlPageforRegistration(final NewUserDTO user, final String username, final String password, final String associateName) {
	  String message = "<!DOCTYPE html>" + "<html>" + "<head>" + "</head>" + "<body  style=\"background-color: #f5f5f5;height: 300px;\">" +

	  "<div style=\"background-color:#2D3E50;height: 50px\" >"
	            + "<div style=\"color: orange;text-align: left;margin-top: 25px;margin-left: 10px\">" + "<h1>MAMS<h1>" + "</div>" + "</div>" +

	  "<div >" + "<div style=\"margin-left: 6px\">" + " Hello <b>" + associateName + "</b>,<br><br></div>"
	            + "<p style=\"text-indent: 5em;\">Your registration has been successfully done.<br>"
	            + "<p style=\"text-indent: 5em;\">Your username is your registered email.<br>"
	            + "<p style=\"text-indent: 5em;\">Your password is as below :<br><br><br>" + "<p style=\"text-indent: 5em;\">password=" + password
	            + "<br><br>" +

	  "<div style=\"background-color:#2D3E50\">" + "<div style=\"color: white;text-align: center;padding:5px;margin-top: 55px;\">"
	            + "Copyright � Aibsinc.com" + "</div>" + "</div>" + "</body>" + "</html>";
	  System.out.println(" HTML message...." + message);

	  return message;
     }

     /**
      * Return the exception to the client
      * 
      * @param exc
      * @return
      */
     @ExceptionHandler(Exception.class)
     public ResponseEntity<String> errorHandler(Exception exc) {
	  LOGGER.info(exc.getMessage(), exc);
	  return new ResponseEntity<>(exc.getMessage(), HttpStatus.BAD_REQUEST);
     }
}
