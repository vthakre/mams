package com.mscs.app.controller;

import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.mscs.app.dto.NewUserDTO;
import com.mscs.app.dto.UserData;
import com.mscs.app.helpers.MailSendService;
import com.mscs.app.helpers.RandomPasswordGenerator;
import com.mscs.app.services.UserService;

/**
 * Rest controller for related of Finance manager rest calls
 * 
 * Date : May 3, 2016
 * 
 * @author Himmat
 */
@Controller
@ComponentScan({ "com.mscs.app.helpers" })
public class FMuserController
{
     private static final Logger LOGGER	= Logger.getLogger(FMuserController.class);

     @Autowired
     UserService		 userService;
     @Autowired
     private JavaMailSender	 mailSender;
     @Autowired
     private MailSendService	 mailSendService;
     @Autowired
     private Environment	 env;

     /**
      * rest for register finance manager
      * 
      * @param user
      * @param request
      * @param response
      * @throws Exception
      */
     @ResponseStatus(HttpStatus.OK)
     @RequestMapping(value = "/FmUser", method = RequestMethod.POST)
     public void createUser(@RequestBody NewUserDTO user, final HttpServletRequest request, HttpServletResponse response) throws Exception {
	  System.out.println("finance manager contrl called successfully.");

	  // Generate random username and password
	  String username = user.getEmail().trim();
	  String password = RandomPasswordGenerator.getRandomPassword().trim();
	  String encodedPassword = new BCryptPasswordEncoder().encode(password);
	  System.out.println("finance manager user username===== " + username);
	  System.out.println("finance manager user password===== " + password);

	  userService.createFinanceManagerUser(user.getrAddressLine1(), user.getrAddressLine2(), user.getrState(), user.getrTown(), user.getrPincode(), username, encodedPassword,
	            user.getUserRole(), user.getMobNo(), true, user.getFullName(), user.getEmail());

	  // send back response string
	  PrintWriter out = response.getWriter();
	  out.print("user registered successfully");
	  out.flush();
	  // generaate html message for registration
	  String htmlMessage = createHtmlPageforRegistration(user, username, password, user.getFullName());
	  // send the email with html message
	  final String subject = "Registration Successful";
	  // send mail throw another thread
	  mailSendService.sendMail(user.getEmail(), htmlMessage, subject);

     }

     /**
      * Method for getting associate user list of approved by Account manager
      * 
      * @param request
      * @param response
      * @throws Exception
      */
     @ResponseStatus(HttpStatus.OK)
     @RequestMapping(value = "/getAssociateUserListForFM", method = RequestMethod.GET)
     public @ResponseBody List<UserData> getAssociateListForFM(final HttpServletRequest request, HttpServletResponse response) throws Exception {

	  // method to get associate user list of approved by Account manager
	  List<UserData> associateUserList = userService.getAssociateListForFM();

	  return associateUserList;

     }

     /**
      * this method is for update or approve associate user by Finance manager
      * 
      * @param user
      * @param request
      * @param response
      * @throws Exception
      */
     @ResponseStatus(HttpStatus.OK)
     @RequestMapping(value = "/approveAssociateUserByFM", method = RequestMethod.POST)
     public void approveAssociateUserByFM(final HttpServletRequest request, HttpServletResponse response) throws Exception {
	  System.out.println("FM contrl called successfully.");
	  long user_id = Long.parseLong(request.getParameter("id"));
	  String category = request.getParameter("category");

	  // set category to associate user
	  userService.approveAssociateUserByFM(user_id, category);

     }

     /**
      * for generate html message.
      * 
      * @param user
      * @param username
      * @param password
      * @param associateName
      * @return
      */
     private String createHtmlPageforRegistration(final NewUserDTO user, final String username, final String password, final String associateName) {
	  String message = "<!DOCTYPE html>" + "<html>" + "<head>" + "</head>" + "<body  style=\"background-color: #f5f5f5;height: 300px;\">" +

	  "<div style=\"background-color:#2D3E50;height: 50px\" >" + "<div style=\"color: orange;text-align: left;margin-top: 25px;margin-left: 10px\">" + "<h1>MAMS<h1>" + "</div>"
	            + "</div>" +

	  "<div >" + "<div style=\"margin-left: 6px\">" + " Hello <b>" + associateName + "</b>,<br><br></div>"
	            + "<p style=\"text-indent: 5em;\">Your registration has been successfully done.<br>"
	            + "<p style=\"text-indent: 5em;\">Your username is your registered email.<br>" + "<p style=\"text-indent: 5em;\">Your password is as below :<br><br><br>"
	            + "<p style=\"text-indent: 5em;\">password=" + password + "<br><br>" +

	  "<div style=\"background-color:#2D3E50\">" + "<div style=\"color: white;text-align: center;padding:5px;margin-top: 55px;\">" + "Copyright � Aibsinc.com" + "</div>"
	            + "</div>" + "</body>" + "</html>";
	  System.out.println(" HTML message...." + message);

	  return message;
     }

     /**
      * Return the exception to the client
      * 
      * @param exc
      * @return
      */
     @ExceptionHandler(Exception.class)
     public ResponseEntity<String> errorHandler(Exception exc) {
	  LOGGER.info(exc.getMessage(), exc);
	  return new ResponseEntity<>(exc.getMessage(), HttpStatus.BAD_REQUEST);
     }
}
