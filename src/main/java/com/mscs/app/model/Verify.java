package com.mscs.app.model;

public class Verify
{

     public static String verify;

     public Verify() {
     }

     public String getVerify() {
	  return verify;
     }

     public void setVerify(String verify) {
	  this.verify = verify;
     }

     @Override
     public String toString() {
	  return "verify [verify=" + verify + "]";
     }

}
