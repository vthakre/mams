
package com.mscs.app.model.tenderRelated;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.mscs.app.model.AbstractEntity;

@Entity
public class Revenue extends AbstractEntity
{

     private String	   revMilestone;
     private String	   revDateP;
     private String	   revAmountP;
     private String	   revDateA;
     private String	   revAmountA;
     @ManyToOne(fetch = FetchType.LAZY)
     @JoinColumn(name = "tenderProject_id")
     private TenderProject tenderProject_id;

     public Revenue() {

     }

     public Revenue( String revMilestone , String revDateP , String revAmountP , String revDateA , String revAmountA , TenderProject tenderProject_id ) {
	  super();
	  this.revMilestone = revMilestone;
	  this.revDateP = revDateP;
	  this.revAmountP = revAmountP;
	  this.revDateA = revDateA;
	  this.revAmountA = revAmountA;
	  this.tenderProject_id = tenderProject_id;
     }

     /**
      * @return the revMilestone
      */
     public String getRevMilestone() {
          return revMilestone;
     }

     /**
      * @param revMilestone the revMilestone to set
      */
     public void setRevMilestone(String revMilestone) {
          this.revMilestone = revMilestone;
     }

     /**
      * @return the revDateP
      */
     public String getRevDateP() {
          return revDateP;
     }

     /**
      * @param revDateP the revDateP to set
      */
     public void setRevDateP(String revDateP) {
          this.revDateP = revDateP;
     }

     /**
      * @return the revAmountP
      */
     public String getRevAmountP() {
          return revAmountP;
     }

     /**
      * @param revAmountP the revAmountP to set
      */
     public void setRevAmountP(String revAmountP) {
          this.revAmountP = revAmountP;
     }

     /**
      * @return the revDateA
      */
     public String getRevDateA() {
          return revDateA;
     }

     /**
      * @param revDateA the revDateA to set
      */
     public void setRevDateA(String revDateA) {
          this.revDateA = revDateA;
     }

     /**
      * @return the revAmountA
      */
     public String getRevAmountA() {
          return revAmountA;
     }

     /**
      * @param revAmountA the revAmountA to set
      */
     public void setRevAmountA(String revAmountA) {
          this.revAmountA = revAmountA;
     }

     /**
      * @return the tenderProject_id
      */
     public TenderProject getTenderProject_id() {
          return tenderProject_id;
     }

     /**
      * @param tenderProject_id the tenderProject_id to set
      */
     public void setTenderProject_id(TenderProject tenderProject_id) {
          this.tenderProject_id = tenderProject_id;
     }

     /* (non-Javadoc)
      * @see java.lang.Object#toString()
      */
     @Override
     public String toString() {
	  return "Revenue [revMilestone=" + revMilestone + ", revDateP=" + revDateP + ", revAmountP=" + revAmountP + ", revDateA=" + revDateA + ", revAmountA=" + revAmountA
	            + ", tenderProject_id=" + tenderProject_id + "]";
     }

     
   }
