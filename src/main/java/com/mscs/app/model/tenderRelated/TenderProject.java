
package com.mscs.app.model.tenderRelated;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import com.mscs.app.model.AbstractEntity;

@Entity
public class TenderProject extends AbstractEntity{

    private String tenderId;
    @OneToMany(mappedBy="tenderProject_id",cascade = CascadeType.ALL)
    private List<Revenue> revenues = new ArrayList<Revenue>();
    @OneToMany(mappedBy="tenderProject_id",cascade = CascadeType.ALL)
    private List<Cost> costs = new ArrayList<Cost>();
    private String projectTitle;
    private String projectDescription;
    private String scopeOfWork;
    private String projectDuration;
    private String projectvalue;
    private String projectStartDate;
    private String projectEndDate;
    @ElementCollection
    @CollectionTable(name="user_data", joinColumns=@JoinColumn(name="associateName"))
    @Column(name="associateName_id")
    private List<String> nameOfAssociate = new ArrayList<String>();

    /**
     * 
     * @return
     *     The tenderId
     */
    public String getTenderId() {
        return tenderId;
    }

    /**
     * 
     * @param tenderId
     *     The tenderId
     */
    public void setTenderId(String tenderId) {
        this.tenderId = tenderId;
    }

    /**
     * 
     * @return
     *     The revenues
     */
    public List<Revenue> getRevenues() {
        return revenues;
    }

    /**
     * 
     * @param revenues
     *     The revenues
     */
    public void setRevenues(List<Revenue> revenues) {
        this.revenues = revenues;
    }

    /**
     * 
     * @return
     *     The costs
     */
    public List<Cost> getCosts() {
        return costs;
    }

    /**
     * 
     * @param costs
     *     The costs
     */
    public void setCosts(List<Cost> costs) {
        this.costs = costs;
    }

    /**
     * 
     * @return
     *     The projectTitle
     */
    public String getProjectTitle() {
        return projectTitle;
    }

    /**
     * 
     * @param projectTitle
     *     The projectTitle
     */
    public void setProjectTitle(String projectTitle) {
        this.projectTitle = projectTitle;
    }

    /**
     * 
     * @return
     *     The projectDescription
     */
    public String getProjectDescription() {
        return projectDescription;
    }

    /**
     * 
     * @param projectDescription
     *     The projectDescription
     */
    public void setProjectDescription(String projectDescription) {
        this.projectDescription = projectDescription;
    }

    /**
     * 
     * @return
     *     The scopeOfWork
     */
    public String getScopeOfWork() {
        return scopeOfWork;
    }

    /**
     * 
     * @param scopeOfWork
     *     The scopeOfWork
     */
    public void setScopeOfWork(String scopeOfWork) {
        this.scopeOfWork = scopeOfWork;
    }

    /**
     * 
     * @return
     *     The projectDuration
     */
    public String getProjectDuration() {
        return projectDuration;
    }

    /**
     * 
     * @param projectDuration
     *     The projectDuration
     */
    public void setProjectDuration(String projectDuration) {
        this.projectDuration = projectDuration;
    }

    /**
     * 
     * @return
     *     The projectvalue
     */
    public String getProjectvalue() {
        return projectvalue;
    }

    /**
     * 
     * @param projectvalue
     *     The projectvalue
     */
    public void setProjectvalue(String projectvalue) {
        this.projectvalue = projectvalue;
    }

    /**
     * 
     * @return
     *     The projectStartDate
     */
    public String getProjectStartDate() {
        return projectStartDate;
    }

    /**
     * 
     * @param projectStartDate
     *     The projectStartDate
     */
    public void setProjectStartDate(String projectStartDate) {
        this.projectStartDate = projectStartDate;
    }

    /**
     * 
     * @return
     *     The projectEndDate
     */
    public String getProjectEndDate() {
        return projectEndDate;
    }

    /**
     * 
     * @param projectEndDate
     *     The projectEndDate
     */
    public void setProjectEndDate(String projectEndDate) {
        this.projectEndDate = projectEndDate;
    }

    /**
     * 
     * @return
     *     The nameOfAssociate
     */
    public List<String> getNameOfAssociate() {
        return nameOfAssociate;
    }

    /**
     * 
     * @param nameOfAssociate
     *     The nameOfAssociate
     */
    public void setNameOfAssociate(List<String> nameOfAssociate) {
        this.nameOfAssociate = nameOfAssociate;
    }

/* (non-Javadoc)
 * @see java.lang.Object#toString()
 */
@Override
public String toString() {
     final int maxLen = 5;
     return "TenderProject [tenderId=" + tenderId + ", revenues=" + (revenues != null ? revenues.subList(0, Math.min(revenues.size(), maxLen)) : null) + ", costs="
               + (costs != null ? costs.subList(0, Math.min(costs.size(), maxLen)) : null) + ", projectTitle=" + projectTitle + ", projectDescription=" + projectDescription
               + ", scopeOfWork=" + scopeOfWork + ", projectDuration=" + projectDuration + ", projectvalue=" + projectvalue + ", projectStartDate=" + projectStartDate
               + ", projectEndDate=" + projectEndDate + ", nameOfAssociate="
               + (nameOfAssociate != null ? nameOfAssociate.subList(0, Math.min(nameOfAssociate.size(), maxLen)) : null) + "]";
}
    
    
    

}
