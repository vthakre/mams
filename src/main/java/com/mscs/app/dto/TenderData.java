package com.mscs.app.dto;

/**
 * This pojo class is used for sending tender data to client.
 * 
 * Date : May 10, 2016
 * 
 * @author Himmat
 */
public class TenderData
{
     long   id;
     String tenderId;
     String projectDesccription;
     String contactPersonName;
     String webSite;

     public TenderData( long id , String tenderId , String projectDesccription , String contactPersonName , String webSite ) {
	  super();
	  this.id = id;
	  this.tenderId = tenderId;
	  this.projectDesccription = projectDesccription;
	  this.contactPersonName = contactPersonName;
	  this.webSite = webSite;
     }

     /**
      * @return the id
      */
     public long getId() {
	  return id;
     }

     /**
      * @param id
      *             the id to set
      */
     public void setId(long id) {
	  this.id = id;
     }

     /**
      * @return the tenderId
      */
     public String getTenderId() {
	  return tenderId;
     }

     /**
      * @param tenderId
      *             the tenderId to set
      */
     public void setTenderId(String tenderId) {
	  this.tenderId = tenderId;
     }

     /**
      * @return the projectDesccription
      */
     public String getProjectDesccription() {
	  return projectDesccription;
     }

     /**
      * @param projectDesccription
      *             the projectDesccription to set
      */
     public void setProjectDesccription(String projectDesccription) {
	  this.projectDesccription = projectDesccription;
     }

     /**
      * @return the contactPersonName
      */
     public String getContactPersonName() {
	  return contactPersonName;
     }

     /**
      * @param contactPersonName
      *             the contactPersonName to set
      */
     public void setContactPersonName(String contactPersonName) {
	  this.contactPersonName = contactPersonName;
     }

     /**
      * @return the webSite
      */
     public String getWebSite() {
	  return webSite;
     }

     /**
      * @param webSite
      *             the webSite to set
      */
     public void setWebSite(String webSite) {
	  this.webSite = webSite;
     }

     /*
      * (non-Javadoc)
      * 
      * @see java.lang.Object#toString()
      */
     @Override
     public String toString() {
	  return "TenderData [id=" + id + ", tenderId=" + tenderId + ", projectDesccription=" + projectDesccription + ", contactPersonName=" + contactPersonName + ", webSite="
	            + webSite + "]";
     }

}
