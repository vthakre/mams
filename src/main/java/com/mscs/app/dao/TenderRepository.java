package com.mscs.app.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mscs.app.dto.TenderData;
import com.mscs.app.model.TendarMaster;
import com.mscs.app.model.tenderRelated.TenderProject;

/**
 *
 * Repository class for the TendorMaster entity
 *
 */
@Repository
public class TenderRepository
{

     @Autowired
     private SessionFactory sessionFactory;

     /**
      * used to get current hibernate session
      * 
      * @return
      */
     private Session getCurrentSession() {

	  return sessionFactory.getCurrentSession();

     }

     /**
      * used for save TendarMaster entity
      * 
      * @param tendor
      */
     public void save(TendarMaster tendor) {
	  getCurrentSession().save(tendor);
     }

     /**
      * used for save TendarMaster entity
      * 
      * @param tendor
      */
     public void saveTenderProject(TenderProject project ) {
	  getCurrentSession().save(project);
     }

     /**
      * mehtod for getting list of tender
      * 
      * @return
      */
     public List<TenderData> getTenderList() {
	  // TODO Auto-generated method stub

	  List<TendarMaster> tenderMasterList = getCurrentSession().createQuery(TendarMaster.ALL_TENDERS).list();
	  //List<TendarMaster> tenderMasterList = getCurrentSession().createQuery(TendarMaster.TENDERS_BY_ID).setParameter("id", 1).list();

	  List<TenderData> tenderList = new ArrayList<>();

	  for (TendarMaster tendM : tenderMasterList) {
	       
	       
	       TenderData td = new TenderData(tendM.getId(), tendM.getTenderId(), tendM.getProjectDesccription(), tendM.getContactPersonName(), tendM.getWebSite());
	  
	       tenderList.add(td);
	  }
	  return tenderList;
     }

     public TendarMaster getTenderById(long id) {
	  // TODO Auto-generated method stub
	  
	  TendarMaster tender=(TendarMaster) getCurrentSession().get(TendarMaster.class, id);
	  
	  return tender;
     }

}
